import { Grid, Text, Input } from "@nextui-org/react";
import React, { useState, useEffect } from "react";
import { Flex } from "../styles/flex";
import { Image } from "@nextui-org/react";
const Fade = require("react-reveal/Fade");

import axios from "axios";

import { Checkbox, Spacer } from "@nextui-org/react";

export const Contact = () => {
  const [flashMessage, setFlashMessage] = useState("");
  const [showMessage, setShowMessage] = useState(false);

  const [email, setEmail] = useState<string>("");
  const [subject, setSubject] = useState<string>("");

  const handleSubmit = async (e: React.FormEvent<HTMLFormElement>) => {
    setFlashMessage("Email Sent !");
    setShowMessage(true);
    e.preventDefault();
    try {
      const response = await axios.post(
        "https://api.kitchen-savvy.com/api/admin/contact-glow",
        {
          email,
          subject,
        },
        {
          headers: {
            "Content-Type": "application/json",
          },
        }
      );
      console.log(response);
    } catch (error) {
      console.error(error);
    }
  };
  useEffect(() => {
    if (showMessage) {
      setTimeout(() => {
        setShowMessage(false);
      }, 8000);
    }
  }, [showMessage]);
  return (
    <>
      <div id="contact">
        <Grid.Container gap={2} justify="center">
          <Grid xs={12} md={12} alignItems="center">
            <Flex
              direction={"column"}
              css={{
                pt: "$20",
                margin: "auto",
                "@sm": {
                  justifyContent: "space-around",
                  flexDirection: "row",
                },

                "@xs": {
                  alignItems: "center",
                },
              }}
            >
              <Flex direction="column">
                <Fade top>
                  <Text className="title" h2>
                    CONTACT US
                  </Text>
                  <Spacer />

                  <form onSubmit={handleSubmit}>
                    <Flex>
                      <Grid
                        css={{
                          margin: "auto",

                          width: "100%",
                        }}
                      >
                        <Input
                          css={{
                            "@xsMax": {
                              width: "80%",
                            },
                          }}
                          bordered
                          labelPlaceholder="Email"
                          color="secondary"
                          type="email"
                          size="lg"
                          width="20em"
                          value={email}
                          onChange={(e) => setEmail(e.target.value)}
                          required
                          clearable
                          contentRightStyling={false}
                        />
                        <Spacer />
                        <Spacer />
                        <Input
                          css={{
                            "@xsMax": {
                              width: "80%",
                            },
                          }}
                          bordered
                          labelPlaceholder="Subject"
                          color="secondary"
                          type="text"
                          size="lg"
                          width="20em"
                          value={subject}
                          onChange={(e) => setSubject(e.target.value)}
                          required
                          clearable
                          contentRightStyling={false}
                        />
                        <Spacer />
                        <Checkbox
                          color="secondary"
                          labelColor="secondary"
                          defaultSelected
                        >
                          Receive updates via our newsletter
                        </Checkbox>
                        <Spacer />
                        <button type="submit" className="main-button gradient">
                          Send
                        </button>
                      </Grid>
                    </Flex>
                    {showMessage ? (
                      <Grid
                        css={{
                          fontWeight: "bold",
                          textGradient: "45deg, #667eea -20%, #6B8DD6 50%",
                          padding: "1em",
                        }}
                      >
                        {flashMessage}
                      </Grid>
                    ) : null}
                  </form>
                </Fade>
              </Flex>

              <Flex
                css={{
                  "@xsMax": {
                    display: "none",
                  },
                }}
              >
                <Fade top>
                  <Image
                    css={{}}
                    width={700}
                    height={500}
                    src="/contact.svg"
                    alt="contact"
                    objectFit="contain"
                  />
                </Fade>
              </Flex>
            </Flex>
          </Grid>
        </Grid.Container>
      </div>
    </>
  );
};
