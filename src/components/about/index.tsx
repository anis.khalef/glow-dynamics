import { Divider, Text } from "@nextui-org/react";
import React from "react";
import { Flex } from "../styles/flex";
const Fade = require("react-reveal/Fade");
import Link from "next/link";
export const About = () => {
  return (
    <>
      <div id="about">
        <Flex
          direction={"column"}
          css={{
            textAlign: "center",
            gap: "1rem",
            pt: "$20",
            justifyContent: "center",
            alignItems: "center",
            px: "$6",
            "@sm": {
              justifyContent: "space-around",
              px: "$32",
              flexDirection: "row",
            },
            "@md": {
              px: "$64",
            },
          }}
        >
          <Flex direction="column">
            <Fade top>
              <Text h2 className="title">
                ABOUT US
              </Text>

              <Flex
                css={{
                  py: "$10",
                  gap: "$5",
                  alignItems: "center",
                }}
              >
                <Flex direction={"column"}>
                  <Text
                    span
                    css={{
                      maxWidth: "800px",
                      color: "$accents8",
                      textAlign: "center",
                    }}
                  >
                    At Glow Dynamics, we specialize in building brands that
                    simplify everyday life. Our flagship brand, Cuisine
                    Pratique, reflects our dedication to creating practical
                    kitchen solutions that resonate with home chefs worldwide.
                    As we expand globally, Cuisine Pratique aims to
                    revolutionize the way people cook, offering innovative
                    products that make kitchens more efficient, no matter the
                    culture or country. With a strategy focused on scaling
                    through multiple international stores, Glow Dynamics is
                    transforming ecommerce and shaping the future of home
                    cooking.
                  </Text>
                </Flex>
              </Flex>
            </Fade>
          </Flex>
        </Flex>
      </div>
      <Divider
        css={{ position: "absolute", inset: "0p", left: "0", mt: "$5" }}
      />
    </>
  );
};
