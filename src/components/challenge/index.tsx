import { Divider, Grid, Text, Button, Spacer } from "@nextui-org/react";
import React from "react";
import { Box } from "../styles/box";
import { Flex } from "../styles/flex";
import { Image } from "@nextui-org/react";
const Fade = require("react-reveal/Fade");
const Bounce = require("react-reveal/Bounce");
const Pulse = require("react-reveal/Pulse");
import Link from "next/link";
export const Challenge = () => {
  return (
    <>
      <div id="about">
        <Flex
          direction={"column"}
          css={{
            textAlign: "center",
            gap: "1rem",
            pt: "$20",
            justifyContent: "center",
            alignItems: "center",
            px: "$6",
            "@sm": {
              justifyContent: "space-around",
              px: "$32",
              flexDirection: "row",
            },
            "@md": {
              px: "$64",
            },
          }}
        >
          <Flex direction="column">
            <Fade top>
              <Text h2 className="title">
                OUR CHALLENGE
              </Text>

              <Flex
                css={{
                  py: "$10",
                  gap: "$5",
                  alignItems: "center",
                }}
              >
                <Flex direction={"column"}>
                  <Text
                    span
                    css={{
                      maxWidth: "800px",
                      color: "$accents8",
                      textAlign: "center",
                    }}
                  >
                    Our mission is to create a seamless and unified brand that
                    resonates across multiple cultures while maintaining a
                    consistent focus on practicality and quality. Each market
                    brings unique challenges, from understanding local consumer
                    needs to navigating diverse regulations and logistics.
                    However, we are determined to grow Cuisine Pratique into an
                    international brand known for empowering home chefs
                    everywhere. Our challenge is to continuously innovate and
                    adapt, while staying true to our core values of simplicity
                    and customer satisfaction.
                  </Text>
                </Flex>
              </Flex>
            </Fade>
          </Flex>
        </Flex>
      </div>
      <Divider
        css={{ position: "absolute", inset: "0p", left: "0", mt: "$5" }}
      />
    </>
  );
};
