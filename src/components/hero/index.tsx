import { Button, Divider, Input, Text } from "@nextui-org/react";
import React, { useState } from "react";
import { Box } from "../styles/box";
import { Flex } from "../styles/flex";
import { Image } from "@nextui-org/react";
import Link from "next/link";
const Bounce = require("react-reveal/Bounce");
const Fade = require("react-reveal/Fade");
import { Spacer } from "@nextui-org/react";
export const Hero = () => {
  return (
    <>
      <Flex
        css={{
          flexDirection: "column",
          alignContent: "center",
          justifyContent: "center",
          alignItems: "center",
          width: "100%",
          padding: "2em",
          "@sm": {
            flexDirection: "row",
          },
        }}
      >
        <Box
          css={{
            pt: "7em",
            display: "flex",
            flexDirection: "column",
          }}
        >
          <Box
            css={{
              maxWidth: "660px",
            }}
          >
            <Text
              h2
              css={{
                textGradient: "45deg, #e463ce -20%, #944de6 40%",
              }}
            >
              CRUSHING ECOMMERCE LIKE A STEAMROLLER
            </Text>
            <Spacer />
            <Fade top>
              <Text
                h4
                css={{
                  fontWeight: 500,
                }}
              >
                At Glow Dynamics, Cuisine Pratique is our flagship brand,
                designed to enhance everyday cooking experiences worldwide. With
                a clear focus on practical kitchen solutions, we are scaling our
                brand internationally, bringing innovation and simplicity to
                kitchens across different cultures.
              </Text>
            </Fade>

            <Bounce top>
              <Text
                h2
                css={{
                  width: "100%",
                  textGradient: "45deg, #e463ce -20%, #944de6 40%",
                  textAlign: "left",
                }}
              >
                <Spacer />
                Our Brand: Cuisine Pratique
              </Text>
            </Bounce>
          </Box>

          <Flex
            css={{
              gap: "$8",
              pt: "$4",
            }}
            wrap={"wrap"}
          >
            <Link href="#contact">
              <button className="main-button gradient">Contact us</button>
            </Link>
          </Flex>
          <br></br>
        </Box>
      </Flex>
      <Divider
        css={{ position: "absolute", inset: "0p", left: "0", mt: "$10" }}
      />
    </>
  );
};
