import { Button, Grid, Text } from "@nextui-org/react";
import React, { useState, useEffect }  from "react";
import { Flex } from "../styles/flex";
import { Checkbox } from "@nextui-org/react";
import axios from "axios";
import { Input, Loading } from "@nextui-org/react";
import { SendButton } from "./SendButton";
import { SendIcon } from "./SendIcon";
export const Trial = () => {
  const [flashMessage, setFlashMessage] = useState("");
  const [showMessage, setShowMessage] = useState(false);

 const [email, setEmail] = useState<string>("");

 const handleSubmit = async (e: React.FormEvent<HTMLFormElement>) => {
    setFlashMessage("A confirmation email has been sent to you !");
    setShowMessage(true);
   e.preventDefault();
   try {
     const response = await axios.post(
       "https://prolific-backend.app/api/subscribe",
       { email }
     );
     console.log(response);
   } catch (error) {
     console.error(error);
   }
 };
  useEffect(() => {
    if (showMessage) {
      setTimeout(() => {
        setShowMessage(false);
      }, 8000); // 5 second delay
    }
  }, [showMessage]);
  return (
    <>
      {/* <Flex className="bg"> </Flex> */}
      <Flex
        css={{
          py: "$20",
          px: "$6",
        }}
        justify={"center"}
        direction={"column"}
        align={"center"}
      >
        <Text
          h3
          css={{
            display: "inline",
            textGradient: "45deg, #667eea -20%, #6B8DD6 50%",
          }}
        >
          Want to stay up to date with the latest SEO trends and techniques?
          <br></br>Subscribe to our newsletter and get insider access.
        </Text>

        <form onSubmit={handleSubmit}>
          <Flex
            css={{
              gap: "$8",
              pt: "$4",
            }}
            wrap={"wrap"}
          >
            <Grid>
              <Input
                type="email"
                placeholder="Enter your email address"
                size="lg"
                value={email}
                onChange={(e) => setEmail(e.target.value)}
                required
                clearable
                contentRightStyling={false}
                contentRight={
                  <SendButton>
                    <SendIcon
                      filled={undefined}
                      size={undefined}
                      height={undefined}
                      width={undefined}
                      label={undefined}
                      className={undefined}
                      type="submit"
                    />
                  </SendButton>
                }
              />
            </Grid>
          </Flex>
        </form>
        {showMessage ? (
          <Grid
            css={{
              fontWeight: "bold",
              textGradient: "45deg, #667eea -20%, #6B8DD6 50%",
              padding: "1em",
            }}
          >
            {flashMessage}
          </Grid>
        ) : null}
      </Flex>
    </>
  );
};
