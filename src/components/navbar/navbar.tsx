import {Button, Dropdown, Link, Navbar, Switch, Text} from '@nextui-org/react';
import React from 'react';
import {icons} from './icons';
import { Image } from '@nextui-org/react';
import {useTheme as useNextTheme} from 'next-themes';
import {useTheme} from '@nextui-org/react';

export const Nav = () => {

   const collapseItems = [
     {
       href: "/",
     },
     {
       href: "#about",
       title: "About",
     },
     {
       href: "#team",
       title: "Team",
     },
     {
       href: "#products",
       title: "Products",
     },
     {
       href: "#partners",
       title: "Partners",
     },
     {
       href: "#contact",
       title: "Contact",
     },
   ];

   return (
     <Navbar
       variant="sticky"
       css={{
         "& .nextui-navbar-container": {
           background: "tranparent",
           border: "none",
           position: "fixed",
           top: 0,
           justifyContent: "space-between",
           margin: "auto",
           width: "70%",
           textAlign: "center",
         },
       }}
     >
       <Navbar.Brand>
         <Navbar.Toggle aria-label="toggle navigation" showIn="xs" />
         <Link href="/">
           <Image
             width={70}
             height={80}
             src="/logo.png"
             alt="logo"
             objectFit="contain"
           />
         </Link>

         <Navbar.Content
           hideIn="xs"
           css={{
             pl: "6rem",
           }}
           enableCursorHighlight
           activeColor="secondary"
         >
           <Navbar.Link isActive href="/">
             Home
           </Navbar.Link>
           <Navbar.Link href="#about">About</Navbar.Link>
           <Navbar.Link href="#team">Team</Navbar.Link>{" "}
           <Navbar.Link href="#products">Products</Navbar.Link>
           <Navbar.Link href="#partners">Partners</Navbar.Link>
           <Navbar.Link href="#contact">Contact</Navbar.Link>
         </Navbar.Content>
       </Navbar.Brand>

       <Navbar.Collapse css={{ position: "fixed" }}>
         {collapseItems.map(({ title, href }) => (
           <Navbar.CollapseItem key={title}>
             <Link
               color="inherit"
               css={{
                 minWidth: "100%",
               }}
               href={href}
             >
               {title}
             </Link>
           </Navbar.CollapseItem>
         ))}
       </Navbar.Collapse>
     </Navbar>
   );
};
