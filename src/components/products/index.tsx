import { Divider, Grid, Text, Input } from "@nextui-org/react";
import React, {useState, useEffect} from "react";

import { Image } from "@nextui-org/react";
const Fade = require("react-reveal/Fade");

import Link from "next/link";

import { Checkbox, Spacer } from "@nextui-org/react";

export const Products = () => {
  
  return (
    <>
      <div id="products">
        <Text
          className="title"
          h2
          css={{
            pt: "3em",
            textAlign: "center",
          }}
        >
          PRODUCTS
        </Text>
        <Spacer /> <Spacer />
        <Grid>
          <Grid>
            <Link href="https://tweetshirtusa.etsy.com">
              <a target="_blank">
                <Fade right>
                  <Grid
                    css={{
                      "@xsMax": {
                        display: "none",
                      },
                    }}
                  >
                    <Image
                      width={1000}
                      height={300}
                      src="/products1.jpg"
                      alt="products1"
                      objectFit="cover"
                    />
                  </Grid>
                </Fade>
                <Spacer /> <Spacer />
                <Fade left>
                  <Grid>
                    <Image
                      width={1000}
                      height={300}
                      src="/products2.jpg"
                      alt="products2"
                      objectFit="contain"
                    />
                  </Grid>
                </Fade>
              </a>
            </Link>
          </Grid>
          <Spacer /> <Spacer />
          <Divider
            css={{
              position: "absolute",
              inset: "0p",
              left: "0",
              mt: "$15",
              linearGradient: "#6727AF ,#8D297C  ",
            }}
          />
          <Spacer /> <Spacer />
          <Spacer /> <Spacer />
          <Fade right>
            <Grid>
              <Link href="https://familymugscreations.etsy.com">
                <a target="_blank">
                  <Image
                    width={1000}
                    height={300}
                    src="/products3.jpg"
                    alt="products3"
                    objectFit="contain"
                  />
                </a>
              </Link>
            </Grid>
          </Fade>
          <Divider
            css={{
              position: "absolute",
              inset: "0p",
              left: "0",
              mt: "$15",
              linearGradient: "#6727AF ,#8D297C  ",
            }}
          />
          <Spacer /> <Spacer /> <Spacer /> <Spacer />{" "}
          <Fade left>
            <Link href="https://inspirationaltshirt.etsy.com">
              <a target="_blank">
                <Image
                  width={1000}
                  height={300}
                  src="/products4.jpg"
                  alt="products"
                  objectFit="contain"
                />
              </a>
            </Link>
          </Fade>
          <Spacer /> <Spacer />
          <Text h2 css={{ textAlign: "center" }}>
            We work on dozens of other stores, each with a specific and highly
            sought-after niche...
          </Text>
        </Grid>
        <Divider
          css={{ position: "absolute", inset: "0p", left: "0", mt: "$15" }}
        />
      </div>
    </>
  );
};
