import Head from "next/head";

 const Meta = ({ title, keywords, description, ogUrl }) => {


  return (
    <>
      <Head>

        <title>{title}</title>
        <meta name="description" content={description} />
        <meta
          name="keywords"
          content={keywords}
        />

        <link rel="apple-touch-icon" sizes="180x180" href="/apple-touch-icon.png"/>
        <link rel="icon" type="image/png" sizes="32x32" href="/favicon-32x32.png" />
        <link rel="icon" type="image/png" sizes="16x16" href="/favicon-16x16.png" />
        <link rel="manifest" href="/site.webmanifest" />
        <link rel="mask-icon" href="/safari-pinned-tab.svg" color="#5bbad5" />
        <meta name="theme-color" content="#ffffff" />
        <meta name="robots" content="index, follow" />
        <meta httpEquiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="language" content="English" />
        <meta name="author" content="Glow Dynamics" />
        <meta
          name="copyright"
          content="All rights reserved, 2023. Glow Dynamics"
        />
        <meta httpEquiv="content-language" content="en" />

        {/* Open Graph / Facebook */}
        <meta property="og:locale" content="en_US" />
        <meta property="og:type" content="website" />
        <meta property="og:title" content={title} />
        <meta property="og:description" content={description} />
        <meta property="og:url" content={ogUrl} />
        <meta property="og:site_name" content={title} />

      </Head>

    </>
  );

};

export default Meta;

Meta.defaultProps = {
  title: "GLOW Dynamics LLC",
  keywords: "Printing company",
  description: "GLOW Dynamics LLC, best on market Printing company",
}

