import { Divider, Text, Grid, Spacer } from "@nextui-org/react";
import React from "react";
import { Image } from "@nextui-org/react";
import { Box } from "../styles/box";
import { Flex } from "../styles/flex";
import Link from "next/link";
export const Footer = () => {
  return (
    <Flex
      css={{
        "@xs": {
          p: "$15",
        },
      }}
    >
      <Divider css={{ position: "absolute", inset: "0p", left: "0" }} />
      <Box as={"footer"} css={{ width: "100%", margin: "auto" }}>
        <Box
          css={{
            px: "$12",
            py: "$10",
          }}
        >
          <Flex
            align={"center"}
            wrap={"wrap"}
            css={{
              justifyContent: "center",
            }}
          >
            <Flex wrap={"wrap"}>
              <Image
                width={150}
                height={100}
                src="/logo.png"
                alt="logo"
                objectFit="contain"
              />
              <br></br>
              <Grid>
                <Text span>Glow Dynamics LLC </Text>
                <br></br>
                <Text span>EIN : 30-1338475</Text>
                <br></br>
                <Text span>30 N Gould St Ste R Sheridan</Text>
                <br></br>
                <Text span>WY 82801, United States</Text>
                <br></br>
                <Text span>contact@glowdynamics.us</Text>
                <br></br>

                <Text span>ceo@glowdynamics.us</Text>
              </Grid>
            </Flex>

            <Grid.Container
              alignItems="center"
              justify="center"
              css={{
                width: "50%",
                margin: "auto",
                "@sm": {
                  width: "100%",
                },
                "&  span": {
                  whiteSpace: "pre",
                },
              }}
            ></Grid.Container>
            <Grid
              css={{
                "@xsMax": {
                  margin: "1em",
                  width: "100%",
                },
              }}
            >
              <Link href="/privacy-policy">
                <Text span css={{ color: "$accents8", cursor: "pointer" }}>
                  Privacy Policy
                </Text>
              </Link>
            </Grid>

            <Spacer />
            <Spacer />
            <Grid
              css={{
                "@xsMax": {
                  margin: "0.5em",
                  textAlign: "center",
                },
              }}
            >
              <Spacer />
              <Spacer />
              <Spacer />

              <Text
                span
                css={{
                  "@xsMax": {},
                }}
              >
                © 2024 Copyright. Glow Dynamics LLC. All rights reserved.
              </Text>
            </Grid>
          </Flex>
        </Box>
      </Box>
    </Flex>
  );
};
