import { Divider, Grid, Text, Button, Spacer } from "@nextui-org/react";
import React from "react";
import { Box } from "../styles/box";
import { Flex } from "../styles/flex";
import { Image } from "@nextui-org/react";
const Fade = require("react-reveal/Fade");
const Bounce = require("react-reveal/Bounce");
const Pulse = require("react-reveal/Pulse");
export const Roadmap = () => {
  return (
    <>
      <div id="about">
        <Flex
          direction={"column"}
          css={{
            textAlign: "left",
            gap: "1rem",
            pt: "$20",
            justifyContent: "center",
            alignItems: "center",
            px: "$6",
            "@sm": {
              justifyContent: "space-around",
              px: "$32",
              flexDirection: "row",
            },
            "@md": {
              px: "$64",
            },
          }}
        >
          <Flex direction="column" align={"center"}>
            <Fade top>
              <Text h2 className="title">
                ROAD MAP
              </Text>
              <Text
                span
                css={{
                  maxWidth: "800px",
                  color: "$accents8",
                  textAlign: "center",
                }}
              >
                We have successfully launched Cuisine Pratique France and
                Cuisine Pratique Polska, and we are excited to expand further.
                <br></br>
                Every two months, we plan to bring Cuisine Pratique to a new
                country. <br></br>
                --{" "}
                <a
                  href="https://cuisine-pratique.com"
                  target="_blank"
                  rel="noreferrer"
                >
                  Cuisine Pratique France{" "}
                </a>
                Since 2019
                <br></br>- 2022 Revenue : 43000 €<br></br>- 2023 Revenue :
                118000 €<br></br>
                --{" "}
                <a
                  href="https://cuisine-pratique.pl"
                  target="_blank"
                  rel="noreferrer"
                >
                  Cuisine Pratique Polska{" "}
                </a>
                Just launched (2024)
                <br></br>
                <br></br>
                Here&apos;s our upcoming schedule: <br></br> <br></br>
              </Text>
              <Text
                span
                css={{
                  maxWidth: "800px",
                  color: "$accents8",
                  textAlign: "left",
                }}
              >
                -- Cuisine Pratique Sverige (coming soon)<br></br> -- Cuisine
                Pratique Norge (coming soon)<br></br> -- Cuisine Pratique
                Danmark (coming soon)<br></br>
                -- Cuisine Pratique Suomi (coming soon)<br></br> -- Cuisine
                Pratique España (coming soon)<br></br> -- Cuisine Pratique
                Portugal (coming soon)<br></br> -- Cuisine Pratique Italia
                (coming soon)
                <br></br> -- Cuisine Pratique Nederland (coming soon)<br></br>{" "}
                -- Cuisine Pratique Magyarország (coming soon)<br></br> --
                Cuisine Pratique Česko (coming soon)<br></br> -- Cuisine
                Pratique Deutschland (coming soon)<br></br> -- Cuisine Pratique
                USA (coming soon)
              </Text>
              <br></br> <br></br>{" "}
            </Fade>
          </Flex>
        </Flex>
      </div>
      <Divider
        css={{ position: "absolute", inset: "0p", left: "0", mt: "$5" }}
      />
    </>
  );
};
