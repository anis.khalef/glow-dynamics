import { Divider, Grid, Spacer, Text } from "@nextui-org/react";
import React from "react";

import { Image } from "@nextui-org/react";
const Fade = require("react-reveal/Fade");

export const Partners = () => {
  return (
    <>
      <div id="partners">
        <Spacer />
        <Spacer />
        <Grid
          css={{
            gap: "1rem",
            pt: "$20",
            justifyContent: "center",
            alignItems: "center",
            px: "$6",
          }}
        >
          <Fade top>
            <Text h2 className="title" css={{ textAlign: "center" }}>
              OUR PARTNERS
            </Text>

            <Grid
              css={{
                "@md": {
                  display: "flex",
                },
                justifyContent: "space-around",
                alignItems: "center",
              }}
            >
              <Grid xs={12}>
                <Image
                  className="partners"
                  alt="paypal"
                  src="/paypal.png"
                  width={180}
                  height={180}
                  objectFit="contain"
                />
              </Grid>

              <Grid xs={12}>
                <Image
                  className="partners"
                  alt="shopify"
                  src="/shopify.png"
                  width={240}
                  height={180}
                  objectFit="contain"
                />
              </Grid>
              <Grid xs={12}>
                <Image
                  className="partners"
                  alt="stripe"
                  src="/stripe.png"
                  width={200}
                  height={180}
                  objectFit="contain"
                />
              </Grid>
              <Grid xs={12}>
                <Image
                  className="partners"
                  alt="dsers"
                  src="/dsers.png"
                  width={200}
                  height={180}
                  objectFit="contain"
                />
              </Grid>
            </Grid>
          </Fade>
        </Grid>
      </div>
      <Spacer />
      <Spacer />

      <Divider
        css={{ position: "absolute", inset: "0p", left: "0", mt: "$5" }}
      />
    </>
  );
};
