import { Button, Grid, Divider, Text, Card } from "@nextui-org/react";
import React, { useState } from "react";
import { Flex } from "../styles/flex";
import Link from "next/link";
const Fade = require("react-reveal/Fade");
import { Image } from "@nextui-org/react";

export const Team = () => {
  const cards = [
    {
      title: "Mehdi Amari",
      post: "Founder and CEO",
      link: "https://www.linkedin.com/in/mehdiamari",
      src: "/linkedin.svg",
    },
  ];
  return (
    <>
      <div id="team">
        <Flex
          direction={"column"}
          justify={"center"}
          align={"center"}
          css={{
            pt: "$20",
          }}
        >
          <Text h2 className="title">
            FOUNDER & CEO
          </Text>
        </Flex>
        <Flex
          align={"center"}
          wrap={"wrap"}
          justify={"center"}
          css={{
            gap: "2rem",
            pt: "$8",
            "@xsMax": {
              padding: "2em",
            },
          }}
        >
          {cards.map((card, index) => (
            <Card className="card" key={index}>
              <Card.Body>
                <Flex css={{ gap: "0.5rem" }}>
                  <Flex direction={"column"}>
                    <Flex direction={"column"}>
                      <Text h4 css={{ textAlign: "center", fontSize: "30px" }}>
                        {card.title}
                      </Text>
                    </Flex>
                    <Flex direction={"column"}>
                      <Text h5 css={{ textAlign: "center", fontSize: "20px" }}>
                        {card.post}
                      </Text>
                    </Flex>
                  </Flex>
                </Flex>
              </Card.Body>
              <Flex>
                <Link href={card.link}>
                  <a target="_blank">
                    <Image
                      width={50}
                      height={100}
                      src={card.src}
                      alt=""
                      objectFit="contain"
                    />
                  </a>
                </Link>
              </Flex>
            </Card>
          ))}
        </Flex>
      </div>

      <Divider
        css={{ position: "absolute", inset: "0p", left: "0", mt: "$15" }}
      />
    </>
  );
};
