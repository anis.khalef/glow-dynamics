import type { NextPage } from "next";
import { Hero } from "@/components/hero";
import { Box } from "@/components/styles/box";
import { Partners } from "@/components/partners";
import { About } from "@/components/about";
import { Challenge } from "@/components/challenge";
import { Roadmap } from "@/components/roadmap";
import { Team } from "@/components/team";
import { Contact } from "@/components/contact";

import Meta from "@/components/seo/index";

import React, { useEffect, useRef, useState } from "react";

const Home: NextPage = () => {
  return (
    <>
      <Meta
        title="Glow Dynamics LLC"
        description="Glow Dynamics LLC"
        ogUrl="https://glowdynamics.us"
        keywords="printing service, mugs prints, t-shirts prints, phonecases prints"
      ></Meta>
      <Box as="main">
        <Hero />
        <About />
        <Challenge />
        <Roadmap />
        <Team />
        <Partners />
        <Contact />
      </Box>
    </>
  );
};

export async function getStaticProps() {
  return { props: {} };
}

export default Home;
